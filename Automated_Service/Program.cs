﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automated_Service
{
    class Program
    {
        #region Private Variables
        private static int _zero = 0;
        private static int _one = 1;
        private static int _twentyFive = 25;
        private static int _division = 0;
        private static int _multiplication = 0;
        private static int _addition = 0;
        private static int _substraction = 0; 
        #endregion

        static void Main(string[] args)
        {
            Console.Title = "Automated Service";
            Common.serviceNotifier.ServiceStart("Automated Service");

                Common.serviceNotifier.DebugStart("Arithmetic Operations");
                try
                {
                    _division = _twentyFive / _one;
                    _multiplication = _twentyFive * _one;
                    _addition = _twentyFive + _one;
                    _substraction = _twentyFive - _one;

                    Common.serviceNotifier.Debug(string.Format("Division of the two numbers : {0} / {1} = {2}", _twentyFive, _one, _division));
                    Common.serviceNotifier.Debug(string.Format("Multiplication of the two numbers : {0} * {1} = {2}", _twentyFive, _one, _multiplication));
                    Common.serviceNotifier.Debug(string.Format("Addition of the two numbers : {0} + {1} = {2}", _twentyFive, _one, _addition));
                    Common.serviceNotifier.Debug(string.Format("Substraction of the two numbers : {0} - {1} = {2}", _twentyFive, _one, _substraction));

                    Common.serviceNotifier.Debug(string.Format("Division of the two numbers : {0} / {1} = {2}", _twentyFive, _one, _division), wantToWriteDateTime: true, wantToWriteInNewLine: true);
                    Common.serviceNotifier.Debug(string.Format("Multiplication of the two numbers : {0} * {1} = {2}", _twentyFive, _one, _multiplication), wantToWriteDateTime: true);
                    Common.serviceNotifier.Debug(string.Format("Addition of the two numbers : {0} + {1} = {2}", _twentyFive, _one, _addition), wantToWriteDateTime: true);
                    Common.serviceNotifier.Debug(string.Format("Substraction of the two numbers : {0} - {1} = {2}", _twentyFive, _one, _substraction), wantToWriteDateTime: true);

                    Common.serviceNotifier.Debug(string.Format("Division of the two numbers : {0} / {1} = {2}", _twentyFive, _one, _division), wantToWriteInNewLine: true);
                    Common.serviceNotifier.Debug(string.Format("Division of the two numbers : {0} / {1} = {2}", _twentyFive, _one, _division), wantToWriteDateTime: true);

                    Common.serviceNotifier.Debug(string.Format("Multiplication of the two numbers : {0} * {1} = {2}", _twentyFive, _one, _multiplication), wantToWriteInNewLine: true);
                    Common.serviceNotifier.Debug(string.Format("Multiplication of the two numbers : {0} * {1} = {2}", _twentyFive, _one, _multiplication), wantToWriteDateTime: true);

                    Common.serviceNotifier.Debug(string.Format("Addition of the two numbers : {0} + {1} = {2}", _twentyFive, _one, _addition), wantToWriteInNewLine: true);
                    Common.serviceNotifier.Debug(string.Format("Addition of the two numbers : {0} + {1} = {2}", _twentyFive, _one, _addition), wantToWriteDateTime: true);

                    Common.serviceNotifier.Debug(string.Format("Substraction of the two numbers : {0} - {1} = {2}", _twentyFive, _one, _substraction), wantToWriteInNewLine: true);
                    Common.serviceNotifier.Debug(string.Format("Substraction of the two numbers : {0} - {1} = {2}", _twentyFive, _one, _substraction), wantToWriteDateTime: true);

                    _division = _twentyFive / _zero;    // This operation is for throwing an exception.
                }
                catch (Exception ex)
                {
                    Common.serviceNotifier.Error("An error occurred while performing arithmetic operations.", ex);
                }
                Common.serviceNotifier.DebugEnd("Arithmetic Operations");

            Common.serviceNotifier.ServiceEnd("Automated Service");

            if (Common.serviceNotifier.listOfErrorLogs != null && Common.serviceNotifier.listOfErrorLogs.Count > 0)
                Common.SendErrorMail();
            else
            {
                if (Common.serviceNotifier.listOfWarnLogs != null && Common.serviceNotifier.listOfWarnLogs.Count > 0)
                    Common.SendWarnMail();

                Common.SendSuccessMail();
            }
        }
    }
}
