﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Automated_Service
{
    public class AppSettings
    {
        //--========= >> General >> START >> ====--
        public static bool WantToWriteLog { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["WantToWriteLog"])) ? Convert.ToBoolean(ConfigurationManager.AppSettings["WantToWriteLog"]) : false; } }
        public static string LogFilePath { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["LogFilePath"])) ? Convert.ToString(ConfigurationManager.AppSettings["LogFilePath"]) : Application.StartupPath; } }
        public static string LogFileName { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["LogFileName"])) ? Convert.ToString(ConfigurationManager.AppSettings["LogFileName"]) : string.Empty; } }
        
        public static string SMTPServer { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["SMTPServer"])) ? Convert.ToString(ConfigurationManager.AppSettings["SMTPServer"]) : string.Empty; } }
        public static string SMTPUserName { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["SMTPUserName"])) ? Convert.ToString(ConfigurationManager.AppSettings["SMTPUserName"]) : string.Empty; } }
        public static string SMTPPassword { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["SMTPPassword"])) ? Convert.ToString(ConfigurationManager.AppSettings["SMTPPassword"]) : string.Empty; } }
        public static string SMTPDomain { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["SMTPDomain"])) ? Convert.ToString(ConfigurationManager.AppSettings["SMTPDomain"]) : string.Empty; } }
        public static int SMTPPort { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["SMTPPort"])) ? Convert.ToInt16(ConfigurationManager.AppSettings["SMTPPort"]) : 25; } }
        public static bool EnableSsl { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["EnableSsl"])) ? Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]) : false; } }
        public static string MailFromEmailAddress { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["MailFromEmailAddress"])) ? Convert.ToString(ConfigurationManager.AppSettings["MailFromEmailAddress"]) : string.Empty; } }
        public static string MailFromEmailDisplayName { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["MailFromEmailDisplayName"])) ? Convert.ToString(ConfigurationManager.AppSettings["MailFromEmailDisplayName"]) : string.Empty; } }

        public static bool WantToSendErrorEmail { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["WantToSendErrorEmail"])) ? Convert.ToBoolean(ConfigurationManager.AppSettings["WantToSendErrorEmail"]) : false; } }
        public static bool WantToWriteErrorLogIntoSeparateFile { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["WantToWriteErrorLogIntoSeparateFile"])) ? Convert.ToBoolean(ConfigurationManager.AppSettings["WantToWriteErrorLogIntoSeparateFile"]) : false; } }
        public static bool WantToAttachLogFileInErrorEmail { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["WantToAttachLogFileInErrorEmail"])) ? Convert.ToBoolean(ConfigurationManager.AppSettings["WantToAttachLogFileInErrorEmail"]) : false; } }
        public static string ErrorEmailTo { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["ErrorEmailTo"])) ? Convert.ToString(ConfigurationManager.AppSettings["ErrorEmailTo"]) : string.Empty; } }
        public static string ErrorEmailCC { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["ErrorEmailCC"])) ? Convert.ToString(ConfigurationManager.AppSettings["ErrorEmailCC"]) : string.Empty; } }
        public static string ErrorEmailBcc { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["ErrorEmailBcc"])) ? Convert.ToString(ConfigurationManager.AppSettings["ErrorEmailBcc"]) : string.Empty; } }
        public static string ErrorEmailSubject { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["ErrorEmailSubject"])) ? Convert.ToString(ConfigurationManager.AppSettings["ErrorEmailSubject"]) : "Error"; } }

        public static bool WantToSendWarnEmail { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["WantToSendWarnEmail"])) ? Convert.ToBoolean(ConfigurationManager.AppSettings["WantToSendWarnEmail"]) : false; } }
        public static string WarnEmailTo { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["WarnEmailTo"])) ? Convert.ToString(ConfigurationManager.AppSettings["WarnEmailTo"]) : string.Empty; } }
        public static string WarnEmailCC { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["WarnEmailCC"])) ? Convert.ToString(ConfigurationManager.AppSettings["WarnEmailCC"]) : string.Empty; } }
        public static string WarnEmailBcc { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["WarnEmailBcc"])) ? Convert.ToString(ConfigurationManager.AppSettings["WarnEmailBcc"]) : string.Empty; } }
        public static string WarnEmailSubject { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["WarnEmailSubject"])) ? Convert.ToString(ConfigurationManager.AppSettings["WarnEmailSubject"]) : "Warning"; } }

        public static bool WantToSendSuccessEmail { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["WantToSendSuccessEmail"])) ? Convert.ToBoolean(ConfigurationManager.AppSettings["WantToSendSuccessEmail"]) : false; } }
        public static bool WantToAttachLogFileInSuccessEmail { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["wantToAttachLogFileInSuccessEmail"])) ? Convert.ToBoolean(ConfigurationManager.AppSettings["wantToAttachLogFileInSuccessEmail"]) : false; } }
        public static string SuccessEmailTo { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["SuccessEmailTo"])) ? Convert.ToString(ConfigurationManager.AppSettings["SuccessEmailTo"]) : string.Empty; } }
        public static string SuccessEmailCC { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["SuccessEmailCC"])) ? Convert.ToString(ConfigurationManager.AppSettings["SuccessEmailCC"]) : string.Empty; } }
        public static string SuccessEmailBcc { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["SuccessEmailBcc"])) ? Convert.ToString(ConfigurationManager.AppSettings["SuccessEmailBcc"]) : string.Empty; } }
        public static string SuccessEmailSubject { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["SuccessEmailSubject"])) ? Convert.ToString(ConfigurationManager.AppSettings["SuccessEmailSubject"]) : "Success"; } }

        public static bool IsLive { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["IsLive"])) ? Convert.ToBoolean(ConfigurationManager.AppSettings["IsLive"]) : false; } }
        public static string TestEmailTo { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["TestEmailTo"])) ? Convert.ToString(ConfigurationManager.AppSettings["TestEmailTo"]) : string.Empty; } }
        public static string TestEmailCc { get { return !string.IsNullOrEmpty(Convert.ToString(ConfigurationManager.AppSettings["TestEmailCc"])) ? Convert.ToString(ConfigurationManager.AppSettings["TestEmailCc"]) : string.Empty; } }
        //--========= >> General >> END >> ======--
    }
}
