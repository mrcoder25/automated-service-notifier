﻿using Automated_Service_Notifier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Automated_Service
{
    public class Common
    {
        #region ReadOnly Variables
        readonly static bool _wantToWriteLog = AppSettings.WantToWriteLog;
        readonly static string _logFileDirectoryPath = AppSettings.LogFilePath;
        readonly static string _logFileName = AppSettings.LogFileName;
        readonly static string _assemblyName = Convert.ToString(Assembly.GetCallingAssembly().GetName().Name);
        readonly static bool _wantToWriteExceptionalLogIntoSeparateFile = AppSettings.WantToWriteErrorLogIntoSeparateFile;
        readonly static bool _wantToSendErrorMail = AppSettings.WantToSendErrorEmail;
        readonly static bool _wantToSendWarnMail = AppSettings.WantToSendWarnEmail;
        readonly static bool _wantToSendSuccessMail = AppSettings.WantToSendSuccessEmail;
        readonly static bool _wantToAttachLogFileInErrorMail = AppSettings.WantToAttachLogFileInErrorEmail;
        readonly static bool _wantToAttachLogFileInSuccessMail = AppSettings.WantToAttachLogFileInSuccessEmail;
        #endregion

        #region Instance Of Classes
        private static Mail _objMail;
        readonly static MailCredentials _mailCredentials = new MailCredentials(AppSettings.SMTPServer, AppSettings.SMTPUserName, AppSettings.SMTPPassword, AppSettings.SMTPPort, AppSettings.EnableSsl);
        readonly static Service _service = new Service(_assemblyName, _wantToWriteLog, _logFileDirectoryPath, _logFileName, _wantToWriteExceptionalLogIntoSeparateFile, _wantToSendErrorMail, _wantToSendWarnMail, _wantToSendSuccessMail, _wantToAttachLogFileInErrorMail, _wantToAttachLogFileInSuccessMail);
        readonly static ServiceNotifier _serviceNotifier = new ServiceNotifier(_service);
        public static ServiceNotifier serviceNotifier { get { return _serviceNotifier; } }
        #endregion

        #region Mail supporting methods
        internal static void SendErrorMail()
        {
            try
            {
                string mailType = "Error";
                SetMailParameters(mailType, out _objMail);

                _objMail.mailSubject = AppSettings.ErrorEmailSubject;

                if (serviceNotifier.SendErrorMail(_objMail))
                {
                    serviceNotifier.DebugStart("Send Error Mail");
                    serviceNotifier.Debug("Mail has been sent successfully for error(s).");
                    serviceNotifier.DebugEnd("Send Error Mail");
                }
                else
                {
                    serviceNotifier.DebugStart("Send Error Mail");
                    serviceNotifier.Debug("Mail has not been sent successfully for error(s).");
                    serviceNotifier.DebugEnd("Send Error Mail");
                }
            }
            catch (Exception ex)
            {
                serviceNotifier.DebugStart("Send Error Mail");
                serviceNotifier.Error("An error occurred while sending mail for error(s).", ex);
                serviceNotifier.DebugEnd("Send Error Mail");
            }
        }

        internal static void SendWarnMail()
        {
            try
            {
                string mailType = "Warn";
                SetMailParameters(mailType, out _objMail);

                _objMail.mailSubject = AppSettings.WarnEmailSubject;

                if (serviceNotifier.SendWarnMail(_objMail))
                {
                    serviceNotifier.DebugStart("Send Warning Mail");
                    serviceNotifier.Debug("Mail has been sent successfully for warning(s).");
                    serviceNotifier.DebugEnd("Send Warning Mail");
                }
                else
                {
                    serviceNotifier.DebugStart("Send Warning Mail");
                    serviceNotifier.Debug("Email has not been sent successfully for warning(s).");
                    serviceNotifier.DebugEnd("Send Warning Mail");
                }
            }
            catch (Exception ex)
            {
                serviceNotifier.DebugStart("Send Warning Mail");
                serviceNotifier.Error("An error occurred while sending mail for warning(s).", ex);
                serviceNotifier.DebugEnd("Send Warning Mail");
            }
        }

        internal static void SendSuccessMail()
        {
            try
            {
                string mailType = "Success";
                SetMailParameters(mailType, out _objMail);

                _objMail.mailSubject = AppSettings.SuccessEmailSubject;

                string serviceName = "AutoBilling_FileGeneration_Helper";

                if (serviceNotifier.SendSuccessMail(_objMail, serviceName))
                {
                    serviceNotifier.DebugStart("Send Success Mail");
                    serviceNotifier.Debug("Mail has been sent successfully about the service completed successfully.");
                    serviceNotifier.DebugEnd("Send Success Mail");
                }
                else
                {
                    serviceNotifier.DebugStart("Send Success Mail");
                    serviceNotifier.Debug("Mail has not been sent successfully about the service completed successfully.");
                    serviceNotifier.DebugEnd("Send Success Mail");
                }
            }
            catch (Exception ex)
            {
                serviceNotifier.DebugStart("Send Success Mail");
                serviceNotifier.Error("An error occurred while sending mail about the service completed successfully.", ex);
                serviceNotifier.DebugEnd("Send Success Mail");
            }
        }

        private static void SetMailParameters(string mailType, out Mail _objMail)
        {
            _objMail = new Mail(_mailCredentials);

            try
            {
                _objMail.mailFrom = AppSettings.MailFromEmailAddress;
                _objMail.mailFromDisplayName = AppSettings.MailFromEmailDisplayName;

                if (!AppSettings.IsLive)
                {
                    if (!string.IsNullOrEmpty(AppSettings.TestEmailTo))
                        _objMail.mailTo.AddRange(AppSettings.TestEmailTo.Split(';'));

                    if (!string.IsNullOrEmpty(AppSettings.TestEmailCc))
                        _objMail.mailCC.AddRange(AppSettings.TestEmailCc.Split(';'));
                }
                else
                {
                    switch (mailType)
                    {
                        case "Error":
                            if (!string.IsNullOrEmpty(AppSettings.ErrorEmailTo))
                                _objMail.mailTo.AddRange(AppSettings.ErrorEmailTo.Split(';'));

                            if (!string.IsNullOrEmpty(AppSettings.ErrorEmailCC))
                                _objMail.mailCC.AddRange(AppSettings.ErrorEmailCC.Split(';'));

                            if (!string.IsNullOrEmpty(AppSettings.ErrorEmailBcc))
                                _objMail.mailBcc.AddRange(AppSettings.ErrorEmailBcc.Split(';'));
                            break;
                        case "Warn":
                            if (!string.IsNullOrEmpty(AppSettings.WarnEmailTo))
                                _objMail.mailTo.AddRange(AppSettings.WarnEmailTo.Split(';'));

                            if (!string.IsNullOrEmpty(AppSettings.WarnEmailCC))
                                _objMail.mailCC.AddRange(AppSettings.WarnEmailCC.Split(';'));

                            if (!string.IsNullOrEmpty(AppSettings.WarnEmailBcc))
                                _objMail.mailBcc.AddRange(AppSettings.WarnEmailBcc.Split(';'));
                            break;
                        case "Success":
                            if (!string.IsNullOrEmpty(AppSettings.SuccessEmailTo))
                                _objMail.mailTo.AddRange(AppSettings.SuccessEmailTo.Split(';'));

                            if (!string.IsNullOrEmpty(AppSettings.SuccessEmailCC))
                                _objMail.mailCC.AddRange(AppSettings.SuccessEmailCC.Split(';'));

                            if (!string.IsNullOrEmpty(AppSettings.SuccessEmailBcc))
                                _objMail.mailBcc.AddRange(AppSettings.SuccessEmailBcc.Split(';'));
                            break;
                        default:
                            if (!string.IsNullOrEmpty(AppSettings.TestEmailTo))
                                _objMail.mailTo.AddRange(AppSettings.TestEmailTo.Split(';'));

                            if (!string.IsNullOrEmpty(AppSettings.TestEmailCc))
                                _objMail.mailCC.AddRange(AppSettings.TestEmailCc.Split(';'));
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        #endregion
    }
}
