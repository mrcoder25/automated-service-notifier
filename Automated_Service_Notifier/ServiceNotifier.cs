﻿using Automated_Service_Notifier.Entities;
using Automated_Service_Notifier.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Automated_Service_Notifier
{
    public class ServiceNotifier
    {
        #region Private Variables/Instance
        readonly LogWriter _logWriter;
        private string _assemblyName;
        private bool _wantToSendErrorMail;
        private bool _wantToSendWarnMail;
        private bool _wantToSendSuccessMail;
        private bool _wantToAttachLogFileInErrorMail;
        private bool _wantToAttachLogFileInSuccessMail;
        private List<string> _listLogFilePath { get { return _logWriter.listLogFilePath; } }
        private List<Log> _listOfErrorLogs { get { return _logWriter.listOfErrorLogs; } }
        private List<Log> _listOfWarnLogs { get { return _logWriter.listOfWarnLogs; } }
        #endregion

        #region Public Variables
        public List<Log> listOfErrorLogs { get { return _listOfErrorLogs; } }
        public List<Log> listOfWarnLogs { get { return _listOfWarnLogs; } }
        #endregion

        #region Parameterized Constructor
        /// <summary>
        /// Initializes a new instance of the ServiceNotifier class by using the specified Service class objects.  
        /// </summary>
        public ServiceNotifier(Service service)
        {
            bool wantToWriteLog = service.wantToWriteLog;
            string logFilePath = service.logFileDirectoryPath;
            string LogFileName = service.logFileName;
            bool wantToWriteExceptionalLogIntoSeparateFile = service.wantToWriteExceptionalLogIntoSeparateFile;

            _logWriter = new LogWriter(wantToWriteLog, logFilePath, LogFileName, wantToWriteExceptionalLogIntoSeparateFile);

            if (!string.IsNullOrEmpty(service.assemblyName))
                _assemblyName = service.assemblyName;
            else
            {
                var currentAssembly = Assembly.GetExecutingAssembly();
                var callerAssemblies = new StackTrace().GetFrames()
                                                .Select(x => x.GetMethod().ReflectedType.Assembly).Distinct()
                                                .Where(x => x.GetReferencedAssemblies().Any(y => y.FullName == currentAssembly.FullName));
                var initialAssembly = callerAssemblies.Last();

                _assemblyName = Convert.ToString(initialAssembly.GetName().Name);
            }

            _wantToSendErrorMail = service.wantToSendErrorMail;
            _wantToSendWarnMail = service.wantToSendWarnMail;
            _wantToSendSuccessMail = service.wantToSendSuccessMail;
            _wantToAttachLogFileInErrorMail = service.wantToAttachLogFileInErrorMail;
            _wantToAttachLogFileInSuccessMail = service.wantToAttachLogFileInSuccessMail;
        }
        #endregion

        #region Service Methods
        /// <summary>
        /// Passes the specified message to the WriteLog method of the LogWriter class for writing log in the text file. 
        /// </summary>
        public void ServiceStart(string serviceStartMessage, bool wantToWriteDateTime = true, bool wantToWriteInNewLine = true)
        {
            Log log = new Log();
            log.level = "ServiceStart";
            log.dateTime = DateTime.Now;
            log.logMessage = serviceStartMessage;
            log.wantToWriteDateTime = wantToWriteDateTime;
            log.wantToWriteInNewLine = wantToWriteInNewLine;
            _logWriter.WriteLog(log);
        }

        /// <summary>
        /// Passes the specified message to the WriteLog method of the LogWriter class for writing log in the text file. 
        /// </summary>
        public void ServiceEnd(string serviceEndMessage, bool wantToWriteDateTime = true, bool wantToWriteInNewLine = true)
        {
            Log log = new Log();
            log.level = "ServiceEnd";
            log.dateTime = DateTime.Now;
            log.logMessage = serviceEndMessage;
            log.wantToWriteDateTime = wantToWriteDateTime;
            log.wantToWriteInNewLine = wantToWriteInNewLine;
            _logWriter.WriteLog(log);
        }

        /// <summary>
        /// Passes the specified message to the WriteLog method of the LogWriter class for writing log in the text file. 
        /// </summary>
        public void DebugStart(string debugStartMessage, bool wantToWriteDateTime = true, bool wantToWriteInNewLine = true)
        {
            Log log = new Log();
            log.level = "DebugStart";
            log.dateTime = DateTime.Now;
            log.logMessage = debugStartMessage;
            log.wantToWriteDateTime = wantToWriteDateTime;
            log.wantToWriteInNewLine = wantToWriteInNewLine;
            _logWriter.WriteLog(log);
        }

        /// <summary>
        /// Passes the specified message to the WriteLog method of the LogWriter class for writing log in the text file. 
        /// </summary>
        public void Debug(string debugMessage, bool wantToWriteDateTime = false, bool wantToWriteInNewLine = false)
        {
            Log log = new Log();
            log.level = "Debug";
            log.dateTime = DateTime.Now;
            log.logMessage = debugMessage;
            log.wantToWriteDateTime = wantToWriteDateTime;
            log.wantToWriteInNewLine = wantToWriteInNewLine;
            _logWriter.WriteLog(log);
        }

        /// <summary>
        /// Passes the specified message to the WriteLog method of the LogWriter class for writing log in the text file. 
        /// </summary>
        public void DebugEnd(string debugEndMessage, bool wantToWriteDateTime = true, bool wantToWriteInNewLine = false)
        {
            Log log = new Log();
            log.level = "DebugEnd";
            log.dateTime = DateTime.Now;
            log.logMessage = debugEndMessage;
            log.wantToWriteDateTime = wantToWriteDateTime;
            log.wantToWriteInNewLine = wantToWriteInNewLine;
            _logWriter.WriteLog(log);
        }

        /// <summary>
        /// Passes the specified message to the WriteLog method of the LogWriter class for writing log in the text file. 
        /// </summary>
        public void Warn(string warnMessage, bool wantToWriteDateTime = true, bool wantToWriteInNewLine = true)
        {
            Log log = new Log();
            log.level = "Warn";
            log.dateTime = DateTime.Now;
            log.logMessage = warnMessage;
            log.wantToWriteDateTime = wantToWriteDateTime;
            log.wantToWriteInNewLine = wantToWriteInNewLine;
            _logWriter.WriteLog(log);
        }

        /// <summary>
        /// Passes the specified message to the WriteLog method of the LogWriter class for writing log in the text file. 
        /// </summary>
        public void Error(string errorMessage, Exception errorException, bool wantToWriteDateTime = true, bool wantToWriteInNewLine = true)
        {
            Log log = new Log();
            log.level = "Error";
            log.dateTime = DateTime.Now;
            log.logMessage = errorMessage;
            log.logException = errorException;
            log.wantToWriteDateTime = wantToWriteDateTime;
            log.wantToWriteInNewLine = wantToWriteInNewLine;
            _logWriter.WriteLog(log);
        }

        /// <summary>
        /// Passes the specified Mail class objects by formatting mail body to the SendMail method of the MailManager class for sending error mail. 
        /// </summary>
        public bool SendErrorMail(Mail objMail, List<Log> listOfErrorLogs = null)
        {
            bool isErrorMailSent = false;

            if (_wantToSendErrorMail)
            {
                if (objMail != null && objMail.mailFrom != null && objMail.mailFrom.Count() > 0 && objMail.mailTo != null && objMail.mailTo.Count() > 0)
                {
                    if (listOfErrorLogs == null || listOfErrorLogs.Count < 1)
                        listOfErrorLogs = _listOfErrorLogs;

                    if (listOfErrorLogs != null && listOfErrorLogs.Count > 0)
                    {
                        try
                        {
                            StringBuilder sbFormattedMailBody = new StringBuilder();
                            sbFormattedMailBody = FormatErrorMailBody(listOfErrorLogs);

                            if (sbFormattedMailBody != null && !string.IsNullOrEmpty(Convert.ToString(sbFormattedMailBody)))
                            {
                                objMail.mailBody = Convert.ToString(sbFormattedMailBody);
                                objMail.isMailBodyHtml = true;
                            }

                            if (!string.IsNullOrEmpty(Convert.ToString(objMail.mailBody)))
                            {
                                if (_wantToAttachLogFileInErrorMail && _listLogFilePath != null && _listLogFilePath.Count() > 0)
                                    foreach (string logFilePath in _listLogFilePath)
                                        if (!string.IsNullOrEmpty(logFilePath))
                                            objMail.mailAttachments.Add(new Attachment(logFilePath));

                                MailManager mailManager = new MailManager(objMail.mailCredentials);
                                isErrorMailSent = mailManager.SendMail(objMail);
                            }
                            else
                            {
                                throw new Exception("The member 'body' of the mail object cannot be an empty string.");
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            _logWriter.listOfErrorLogs = new List<Log>();
                        }
                    }
                }
                else
                {
                    throw new Exception("The object 'mail' of the Mail class cannot be an empty member(s).");
                }
            }
            return isErrorMailSent;
        }

        /// <summary>
        /// Passes the specified Mail class objects by formatting mail body to the SendMail method of the MailManager class for sending warning mail. 
        /// </summary>
        public bool SendWarnMail(Mail objMail, List<Log> listOfWarnLogs = null)
        {
            bool isWarnMailSent = false;

            if (_wantToSendWarnMail)
            {
                if (objMail != null && objMail.mailFrom != null && objMail.mailFrom.Count() > 0 && objMail.mailTo != null && objMail.mailTo.Count() > 0)
                {
                    if (listOfWarnLogs == null || listOfWarnLogs.Count < 1)
                        listOfWarnLogs = _listOfWarnLogs;

                    if (listOfWarnLogs != null && listOfWarnLogs.Count > 0)
                    {
                        try
                        {
                            StringBuilder sbFormattedMailBody = new StringBuilder();
                            sbFormattedMailBody = FormatWarnMailBody(listOfWarnLogs);

                            if (sbFormattedMailBody != null && !string.IsNullOrEmpty(Convert.ToString(sbFormattedMailBody)))
                            {
                                objMail.mailBody = Convert.ToString(sbFormattedMailBody);
                                objMail.isMailBodyHtml = true;
                            }

                            if (!string.IsNullOrEmpty(Convert.ToString(objMail.mailBody)))
                            {
                                MailManager mailManager = new MailManager(objMail.mailCredentials);
                                isWarnMailSent = mailManager.SendMail(objMail);
                            }
                            else
                            {
                                throw new Exception("The member 'body' of the mail object cannot be an empty string.");
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            _logWriter.listOfWarnLogs = new List<Log>();
                        }
                    }
                }
                else
                {
                    throw new Exception("The object 'mail' of the Mail class cannot be an empty member(s).");
                }
            }
            return isWarnMailSent;
        }

        /// <summary>
        /// Passes the specified Mail class objects by formatting mail body to the SendMail method of the MailManager class for sending success mail. 
        /// </summary>
        public bool SendSuccessMail(Mail objMail, string serviceName = "")
        {
            bool isSuccessMailSent = false;

            if (_wantToSendSuccessMail)
            {
                if (objMail != null && objMail.mailFrom != null && objMail.mailFrom.Count() > 0 && objMail.mailTo != null && objMail.mailTo.Count() > 0)
                {
                    try
                    {
                        StringBuilder sbFormattedMailBody = new StringBuilder();
                        sbFormattedMailBody = FormatSuccessMailBody(serviceName);

                        if (sbFormattedMailBody != null && !string.IsNullOrEmpty(Convert.ToString(sbFormattedMailBody)))
                        {
                            if (_wantToAttachLogFileInSuccessMail && _listLogFilePath != null && _listLogFilePath.Count() > 0)
                                foreach (string logFilePath in _listLogFilePath)
                                    if (!string.IsNullOrEmpty(logFilePath))
                                        objMail.mailAttachments.Add(new Attachment(logFilePath));

                            objMail.mailBody = Convert.ToString(sbFormattedMailBody);
                            objMail.isMailBodyHtml = true;
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(objMail.mailBody)))
                        {
                            MailManager mailManager = new MailManager(objMail.mailCredentials);
                            isSuccessMailSent = mailManager.SendMail(objMail);
                        }
                        else
                        {
                            throw new Exception("The member 'body' of the mail object cannot be an empty string.");
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    throw new Exception("The object 'mail' of the Mail class cannot be an empty member(s).");
                }
            }
            return isSuccessMailSent;
        }
        #endregion

        #region Suportting Methods
        /// <summary>
        /// Makes body for error mail from the specified list of Error Log(s).
        /// </summary>
        private StringBuilder FormatErrorMailBody(List<Log> listOfErrorLogs)
        {
            string assemblyName = string.IsNullOrEmpty(_assemblyName) ? Convert.ToString(listOfErrorLogs[0].logException.Source) : _assemblyName;
            StringBuilder sbFormattedMailBody = new StringBuilder();

            sbFormattedMailBody.Append("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'><html><head><title></title></head><body><table cellpadding='1' cellspacing='1' style='font-family: calibri; font-size: 11pt; color:#000000; width:745px;'>");
            sbFormattedMailBody.Append("<tr><td style='font-family: calibri; font-size: 11pt; color:#000000;'><b>Hello Team</b>, <br/>We found some error(s) in <b>" + assemblyName + "</b>. Please find below the same.</td></tr>");
            sbFormattedMailBody.Append("<tr><td>&nbsp;</td></tr>");

            sbFormattedMailBody.Append("<tr><td style='font-family: calibri; font-size: 11pt; color:#000000;'><b>Error List</b><br/></td></tr>");
            sbFormattedMailBody.Append("<tr><td><table cellpadding='2' cellspacing='2' style='font-family: calibri; font-size: 11pt; color:#000000; border: 1.5px solid #bfbfbf; width:100%;'>");

            foreach (var errorLog in listOfErrorLogs)
            {
                try
                {
                    sbFormattedMailBody.Append("<tr><td><table border='0' cellpadding='2' cellspacing='2' style='font-family: calibri; font-size: 11pt; color:#000000; background-color:#FCEFEF; border: 0.2px solid #FFD2D2; width:100%;'>");
                    sbFormattedMailBody.Append("<tr><td><b>Date Time</b></td><td align='center'> : </td><td>" + (errorLog.dateTime).ToString("MM-dd-yyyy hh:mm:ss tt") + "</td></tr>");
                    sbFormattedMailBody.Append("<tr><td><b>Log Message</b></td><td align='center'> : </td><td>" + errorLog.logMessage + "</td></tr>");
                    sbFormattedMailBody.Append("<tr><td><b>Region</b></td><td align='center'> : </td><td>" + Convert.ToString(((System.Reflection.MemberInfo)(errorLog.logException.TargetSite)).Name).Trim() + "</td></tr>");
                    sbFormattedMailBody.Append("<tr><td><b>Exception Message</b></td><td align='center'> : </td><td>" + Convert.ToString(errorLog.logException.Message).Trim() + "</td></tr>");
                    sbFormattedMailBody.Append("<tr><td><b>Stack Trace</b></td><td align='center'> : </td><td>" + Convert.ToString(errorLog.logException.StackTrace).Trim() + "</td></tr>");
                    sbFormattedMailBody.Append("</table></td></tr>");
                }
                catch (Exception ex)
                {
                }
            }

            sbFormattedMailBody.Append("</table></td></tr>");
            sbFormattedMailBody.Append("<tr><td>&nbsp;</td></tr>");
            sbFormattedMailBody.Append("<tr><td>Thank You, <br/>System Administrator</td></tr>");
            sbFormattedMailBody.Append("</table></body></html>");

            return sbFormattedMailBody;
        }

        /// <summary>
        /// Makes body for warning mail from the specified list of Warning Log(s).
        /// </summary>
        private StringBuilder FormatWarnMailBody(List<Log> listOfWarnLogs)
        {
            StringBuilder sbFormattedMailBody = new StringBuilder();

            sbFormattedMailBody.Append("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'><html><head><title></title></head><body><table cellpadding='1' cellspacing='1' style='font-family: calibri; font-size: 11pt; color:#000000;'>");
            sbFormattedMailBody.Append("<tr><td style='font-family: calibri; font-size: 11pt; color:#000000;'><b>Hello Team</b>, <br/>We found some warning(s) in <b>" + _assemblyName + "</b>. Please find below the same.</td></tr>");
            sbFormattedMailBody.Append("<tr><td>&nbsp;</td></tr>");

            sbFormattedMailBody.Append("<tr><td style='font-family: calibri; font-size: 11pt; color:#000000;'><b>Warning List</b><br/></td></tr>");
            sbFormattedMailBody.Append("<tr><td><table cellpadding='2' cellspacing='2' style='font-family: calibri; font-size: 11pt; color:#000000; border: 1.5px solid #bfbfbf; width:100%;'>");

            foreach (var warnLog in listOfWarnLogs)
            {
                try
                {
                    sbFormattedMailBody.Append("<tr><td><table border='0' cellpadding='2' cellspacing='2' style='font-family: calibri; font-size: 11pt; color:#000000; background-color:#FEF3D0; border: 0.2px solid #E6DCBC; width:100%;'>");
                    sbFormattedMailBody.Append("<tr><td style='width:110px !important;'><b>Date Time</b></td><td align='center'> : </td><td>" + (warnLog.dateTime).ToString("MM-dd-yyyy hh:mm:ss tt") + "</td></tr>");
                    sbFormattedMailBody.Append("<tr><td style='width:110px !important;'><b>Warning Message</b></td><td align='center'> : </td><td>" + warnLog.logMessage + "</td></tr>");
                    sbFormattedMailBody.Append("</table></td></tr>");
                }
                catch (Exception ex)
                {
                }
            }

            sbFormattedMailBody.Append("</table></td></tr>");
            sbFormattedMailBody.Append("<tr><td>&nbsp;</td></tr>");
            sbFormattedMailBody.Append("<tr><td>Thank You, <br/>System Administrator</td></tr>");
            sbFormattedMailBody.Append("</table></body></html>");

            return sbFormattedMailBody;
        }

        /// <summary>
        /// Makes body for success mail from the specified service name.
        /// </summary>
        private StringBuilder FormatSuccessMailBody(string serviceName)
        {
            if (string.IsNullOrEmpty(serviceName))
                serviceName = "Service";

            DateTime dateTime = DateTime.Now;
            StringBuilder sbFormattedMailBody = new StringBuilder();

            sbFormattedMailBody.Append("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'><html><head><title></title></head><body><table cellpadding='1' cellspacing='1' style='font-family: calibri; font-size: 11pt; color:#000000;'>");
            sbFormattedMailBody.Append("<tr><td style='font-family: calibri; font-size: 11pt; color:#000000;'><b>Hello Team</b>, <br/><b>" + serviceName + "</b> has been run successfully on " + dateTime.ToString("yyyy-MM-dd") + ".</td></tr>");
            sbFormattedMailBody.Append("<tr><td>&nbsp;</td></tr>");

            sbFormattedMailBody.Append("<tr><td><table border='0' cellpadding='2' cellspacing='2' style='font-family: calibri; font-size: 15pt; color:#4F8A10; background-color:#DFF2BF; border: 0.2px solid #A3B97C; width:100%;'>");
            sbFormattedMailBody.Append("<tr><td align='center'><b>Success!</b></td></tr>");
            sbFormattedMailBody.Append("</table></td></tr>");

            sbFormattedMailBody.Append("<tr><td>&nbsp;</td></tr>");
            sbFormattedMailBody.Append("<tr><td>Thank You, <br/>System Administrator</td></tr>");
            sbFormattedMailBody.Append("</table></body></html>");

            return sbFormattedMailBody;
        }
        #endregion

        #region Destructor
        /// <summary>
        /// The Garbage Collection mechanism in .NET will call this method prior to the garbage collection of the objects ServiceNotifier class.
        /// </summary>
        ~ServiceNotifier() { }
        #endregion
    }
}
