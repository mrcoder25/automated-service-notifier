﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automated_Service_Notifier
{
    public class Service
    {
        #region ReadOnly Variables
        readonly string _assemblyName;
        readonly bool _wantToWriteLog;
        readonly string _logFileDirectoryPath;
        readonly string _logFileName;
        readonly bool _wantToWriteExceptionalLogIntoSeparateFile;
        readonly bool _wantToSendErrorMail;
        readonly bool _wantToSendWarnMail;
        readonly bool _wantToSendSuccessMail;
        readonly bool _wantToAttachLogFileInErrorMail;
        readonly bool _wantToAttachLogFileInSuccessMail;
        #endregion

        #region Public Variables
        public string assemblyName { get { return _assemblyName; } }
        public bool wantToWriteLog { get { return _wantToWriteLog; } }
        public string logFileDirectoryPath { get { return _logFileDirectoryPath; } }
        public string logFileName { get { return _logFileName; } }
        public bool wantToWriteExceptionalLogIntoSeparateFile { get { return _wantToWriteExceptionalLogIntoSeparateFile; } }
        public bool wantToSendErrorMail { get { return _wantToSendErrorMail; } }
        public bool wantToSendWarnMail { get { return _wantToSendWarnMail; } }
        public bool wantToSendSuccessMail { get { return _wantToSendSuccessMail; } }
        public bool wantToAttachLogFileInErrorMail { get { return _wantToAttachLogFileInErrorMail; } }
        public bool wantToAttachLogFileInSuccessMail { get { return _wantToAttachLogFileInSuccessMail; } }
        #endregion

        #region Parameterized Constructor
        /// <summary>
        /// Initializes a new instance of the Service class.  
        /// </summary>
        public Service(string assemblyName, bool wantToWriteLog, string logFileDirectoryPath, string logFileName = "LogFile", bool wantToWriteExceptionalLogIntoSeparateFile = false, bool wantToSendErrorMail = false, bool wantToSendWarnMail = false, bool wantToSendSuccessMail = false, bool wantToAttachLogFileInErrorMail = false, bool wantToAttachLogFileInSuccessMail = false)
        {
            _assemblyName = assemblyName;
            _wantToWriteLog = wantToWriteLog;
            _logFileDirectoryPath = logFileDirectoryPath;
            _logFileName = logFileName;
            _wantToWriteExceptionalLogIntoSeparateFile = wantToWriteExceptionalLogIntoSeparateFile;
            _wantToSendErrorMail = wantToSendErrorMail;
            _wantToSendWarnMail = wantToSendWarnMail;
            _wantToSendSuccessMail = wantToSendSuccessMail;
            _wantToAttachLogFileInErrorMail = wantToAttachLogFileInErrorMail;
            _wantToAttachLogFileInSuccessMail = wantToAttachLogFileInSuccessMail;
        }
        #endregion

        #region Destructor
        /// <summary>
        /// The Garbage Collection mechanism in .NET will call this method prior to the garbage collection of the objects Service class.
        /// </summary>
        ~Service() { }
        #endregion
    }
}
