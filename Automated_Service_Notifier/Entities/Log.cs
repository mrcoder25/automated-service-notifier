﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automated_Service_Notifier.Entities
{
    public class Log
    {
        #region Private Variables
        private string _level;
        private DateTime _dateTime;
        private string _logMessage;
        private Exception _logException;
        private bool _wantToWriteInNewLine;
        private bool _wantToWriteDateTime;
        #endregion

        #region Public Variables
        public string level { get { return _level; } set { _level = value; } }
        public DateTime dateTime { get { return _dateTime; } set { _dateTime = value; } }
        public string logMessage { get { return _logMessage; } set { _logMessage = value; } }
        public Exception logException { get { return _logException; } set { _logException = value; } }
        public bool wantToWriteInNewLine { get { return _wantToWriteInNewLine; } set { _wantToWriteInNewLine = value; } }
        public bool wantToWriteDateTime { get { return _wantToWriteDateTime; } set { _wantToWriteDateTime = value; } }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the Log class.   
        /// </summary>
        public Log()
        {
            _level = string.Empty;
            _dateTime = DateTime.Now;
            _logMessage = string.Empty;
            _logException = new Exception();
            _wantToWriteInNewLine = false;
            _wantToWriteDateTime = false;
        }
        #endregion

        #region Destructor
        /// <summary>
        /// The Garbage Collection mechanism in .NET will call this method prior to the garbage collection of the objects Log class.
        /// </summary>
        ~Log() { }
        #endregion
    }
}
