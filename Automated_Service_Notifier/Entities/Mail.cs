﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Automated_Service_Notifier
{
    public class Mail
    {
        #region ReadOnly/Private Variables
        readonly MailCredentials _mailCredentials;
        private string _mailFrom;
        private string _mailFromDisplayName;
        private List<string> _mailTo;
        private List<string> _mailCC;
        private List<string> _mailBcc;
        private List<Attachment> _mailAttachments;
        private string _mailSubject;
        private string _mailBody;
        private bool _isMailBodyHtml;
        #endregion

        #region Public Variables
        public MailCredentials mailCredentials { get { return _mailCredentials; } }
        public string mailFrom { get { return _mailFrom; } set { _mailFrom = value; } }
        public string mailFromDisplayName { get { return _mailFromDisplayName; } set { _mailFromDisplayName = value; } }
        public List<string> mailTo { get { return _mailTo; } set { _mailTo = value; } }
        public List<string> mailCC { get { return _mailCC; } set { _mailCC = value; } }
        public List<string> mailBcc { get { return _mailBcc; } set { _mailBcc = value; } }
        public List<Attachment> mailAttachments { get { return _mailAttachments; } set { _mailAttachments = value; } }
        public string mailSubject { get { return _mailSubject; } set { _mailSubject = value; } }
        public string mailBody { get { return _mailBody; } set { _mailBody = value; } }
        public bool isMailBodyHtml { get { return _isMailBodyHtml; } set { _isMailBodyHtml = value; } }
        #endregion

        #region Parameterized Constructor
        /// <summary>
        /// Initializes a new instance of the Mail class by using the specified MailCredentials class objects.     
        /// </summary>
        public Mail(MailCredentials mailCredentials)
        {
            _mailCredentials = mailCredentials;
            _mailFrom = mailFromDisplayName = _mailSubject = _mailBody = string.Empty;
            _mailTo = new List<string>();
            _mailCC = new List<string>();
            _mailBcc = new List<string>();
            _mailAttachments = new List<Attachment>();
            _isMailBodyHtml = false;
        }
        #endregion

        #region Destructor
        /// <summary>
        /// The Garbage Collection mechanism in .NET will call this method prior to the garbage collection of the objects Mail class.
        /// </summary>
        ~Mail() { }
        #endregion
    }

    public class MailCredentials
    {
        #region ReadOnly Variables
        readonly string _host;
        readonly string _userName;
        readonly string _password;
        readonly string _domain;
        readonly int _port;
        readonly bool _enableSsl;
        readonly bool _useDefaultCredentials;
        #endregion

        #region Public Variables
        public string host { get { return _host; } }
        public string userName { get { return _userName; } }
        public string password { get { return _password; } }
        public string domain { get { return _domain; } }
        public int port { get { return _port; } }
        public bool enableSsl { get { return _enableSsl; } }
        public bool useDefaultCredentials { get { return _useDefaultCredentials; } }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the MailCredentials class.   
        /// </summary>
        public MailCredentials(string host, string userName, string password, int port, bool enableSsl = false, bool useDefaultCredentials = false, string domain = "")
        {
            _host = host;
            _userName = userName;
            _password = password;
            _domain = domain;
            _port = port;
            _enableSsl = enableSsl;
            _useDefaultCredentials = useDefaultCredentials;
        }
        #endregion

        #region Destructor
        /// <summary>
        /// The Garbage Collection mechanism in .NET will call this method prior to the garbage collection of the objects MailCredentials class.
        /// </summary>
        ~MailCredentials() { }
        #endregion
    }
}
