﻿using Automated_Service_Notifier.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Automated_Service_Notifier.Helper
{
    public class MailManager
    {
        #region ReadOnly Variables
        readonly SmtpClient _smtpClient;
        #endregion

        #region Parameterized Constructor
        /// <summary>
        /// Initializes a new instance of the MailManager class.  
        /// </summary>
        public MailManager(MailCredentials mailCredentials)
        {
            _smtpClient = AuthenticateCredentials(mailCredentials);
        }
        #endregion

        #region Mail Suportting Methods
        /// <summary>
        /// Sends the specified mail to an SMTP server for delivery. 
        /// </summary>
        public bool SendMail(Mail mail)
        {
            bool isMailSent = false;

            if (mail != null)
            {
                MailMessage mailMessage = new MailMessage();

                try
                {
                    if (mail.mailTo != null && mail.mailTo.Count > 0)
                        foreach (string to in mail.mailTo)
                            if (!string.IsNullOrEmpty(to))
                                mailMessage.To.Add(to);

                    if (mail.mailCC != null && mail.mailCC.Count > 0)
                        foreach (string CC in mail.mailCC)
                            if (!string.IsNullOrEmpty(CC))
                                mailMessage.CC.Add(CC);

                    if (mail.mailBcc != null && mail.mailBcc.Count > 0)
                        foreach (string Bcc in mail.mailBcc)
                            if (!string.IsNullOrEmpty(Bcc))
                                mailMessage.Bcc.Add(Bcc);

                    if (mail.mailAttachments != null && mail.mailAttachments.Count > 0)
                        foreach (Attachment attachement in mail.mailAttachments)
                            if (attachement != null)
                                mailMessage.Attachments.Add(attachement);

                    if (!string.IsNullOrEmpty(mail.mailFromDisplayName))
                        mailMessage.From = new MailAddress(mail.mailFrom, mail.mailFromDisplayName);
                    else
                        mailMessage.From = new MailAddress(mail.mailFrom);

                    mailMessage.Subject = mail.mailSubject;
                    mailMessage.Body = mail.mailBody;
                    mailMessage.IsBodyHtml = mail.isMailBodyHtml;

                    if (_smtpClient != null)
                    {
                        try
                        {
                            _smtpClient.Send(mailMessage);
                            isMailSent = true;
                        }
                        catch (Exception)
                        {
                            try
                            {
                                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                                _smtpClient.Send(mailMessage);
                                isMailSent = true;
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    mailMessage.Dispose();
                }
            }
            return isMailSent;
        }

        /// <summary>
        /// Authenticates an SMTP server by using the specified MailCredentials class objects. 
        /// </summary>
        private static SmtpClient AuthenticateCredentials(MailCredentials mailCredentials)
        {
            SmtpClient smtpClient = new SmtpClient();
            try
            {
                if (mailCredentials != null)
                {
                    smtpClient.Host = mailCredentials.host;
                    smtpClient.Port = mailCredentials.port;
                    smtpClient.EnableSsl = mailCredentials.enableSsl;
                    if (!string.IsNullOrEmpty(mailCredentials.userName) && !string.IsNullOrEmpty(mailCredentials.password))
                    {
                        smtpClient.UseDefaultCredentials = mailCredentials.useDefaultCredentials;

                        if (!string.IsNullOrEmpty(mailCredentials.domain))
                            smtpClient.Credentials = new NetworkCredential(mailCredentials.userName, mailCredentials.password, mailCredentials.domain);
                        else
                            smtpClient.Credentials = new NetworkCredential(mailCredentials.userName, mailCredentials.password);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return smtpClient;
        }
        #endregion

        #region Destructor
        /// <summary>
        /// The Garbage Collection mechanism in .NET will call this method prior to the garbage collection of the objects MailManager class.
        /// </summary>
        ~MailManager() { }
        #endregion
    }
}
