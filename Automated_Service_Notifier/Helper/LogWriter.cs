﻿using Automated_Service_Notifier.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automated_Service_Notifier.Helper
{
    public class LogWriter
    {
        #region Private Variables
        private bool _wantToWriteLog;
        private string _logFileDirectoryPath;
        private string _logFileName;
        private List<string> _listLogFilePath;
        private bool _wantToWriteExceptionalLogIntoSeparateFile;
        private static List<Log> _listOfErrorLogs;
        private static List<Log> _listOfWarnLogs;
        #endregion

        #region public Variables
        public List<string> listLogFilePath { get { return _listLogFilePath; } }
        public List<Log> listOfErrorLogs { get { return _listOfErrorLogs; } set { _listOfErrorLogs = value; } }
        public List<Log> listOfWarnLogs { get { return _listOfWarnLogs; } set { _listOfWarnLogs = value; } }
        #endregion

        #region Parameterized Constructor
        /// <summary>
        /// Initializes a new instance of the LogWriter class.  
        /// </summary>
        public LogWriter(bool wantToWriteLog, string logFileDirectoryPath, string logFileName = "LogFile", bool wantToWriteExceptionalLogIntoSeparateFile = false)
        {
            _wantToWriteLog = wantToWriteLog;
            _logFileDirectoryPath = string.IsNullOrEmpty(_logFileDirectoryPath) ? !string.IsNullOrEmpty(logFileDirectoryPath) ? logFileDirectoryPath : string.Empty : _logFileDirectoryPath;
            _logFileName = string.IsNullOrEmpty(_logFileName) ? !string.IsNullOrEmpty(logFileName) ? logFileName : "LogFile" : _logFileName;
            _listLogFilePath = new List<string>();
            _wantToWriteExceptionalLogIntoSeparateFile = wantToWriteExceptionalLogIntoSeparateFile;

            if (_listOfErrorLogs == null || _listOfErrorLogs.Count < 1)
                _listOfErrorLogs = new List<Log>();

            if (_listOfWarnLogs == null || _listOfWarnLogs.Count < 1)
                _listOfWarnLogs = new List<Log>();
        }
        #endregion

        #region Log Method
        /// <summary>
        /// Writes the specified Log in the text file.
        /// </summary>
        public void WriteLog(Log log)
        {
            try
            {
                if (log != null)
                {
                    StringBuilder sbFormattedLog = new StringBuilder();
                    sbFormattedLog = FormatLog(log);

                    if (_wantToWriteLog && !string.IsNullOrEmpty(Convert.ToString(sbFormattedLog)))
                    {
                        if (!string.IsNullOrEmpty(_logFileDirectoryPath))
                        {
                            if (!Directory.Exists(_logFileDirectoryPath))
                                Directory.CreateDirectory(_logFileDirectoryPath);

                            if (!_logFileDirectoryPath.EndsWith("\\"))
                                _logFileDirectoryPath = _logFileDirectoryPath + "\\";

                            string logFilePath = string.Empty;

                            if (string.Compare(log.level, "Error", StringComparison.OrdinalIgnoreCase) == 0 && _wantToWriteExceptionalLogIntoSeparateFile)
                                logFilePath = _logFileDirectoryPath + "Exception_" + _logFileName + "_" + (log.dateTime).ToString("yyyy_MM_dd") + ".txt";
                            else
                                logFilePath = _logFileDirectoryPath + _logFileName + "_" + (log.dateTime).ToString("yyyy_MM_dd") + ".txt";

                            using (StreamWriter writer = new StreamWriter(logFilePath, true))
                            {
                                if (!listLogFilePath.Contains(logFilePath))
                                    listLogFilePath.Add(logFilePath);

                                writer.WriteLine(sbFormattedLog);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Suportting Methods
        /// <summary>
        /// Formats the specified Log according log's level.
        /// </summary>
        private StringBuilder FormatLog(Log log)
        {
            StringBuilder sbFormattedLog = new StringBuilder();

            try
            {
                if (log.wantToWriteInNewLine)
                    sbFormattedLog.Append(Environment.NewLine);

                string strMessage = string.Empty;

                switch (log.level)
                {
                    case "ServiceStart":
                        if (log.wantToWriteDateTime)
                            strMessage = string.Format("========= >> {0} >> " + (log.dateTime).ToString("MM-dd-yyyy hh:mm:ss tt") + " >> START >> =======|", log.logMessage);
                        else
                            strMessage = string.Format("========= >> {0} >> START >> =======|", log.logMessage);

                        sbFormattedLog.Append(Repeat("*", strMessage.Length));
                        sbFormattedLog.Append(Environment.NewLine + strMessage);
                        sbFormattedLog.Append(Environment.NewLine + Repeat("*", strMessage.Length));
                        break;
                    case "ServiceEnd":
                        if (log.wantToWriteDateTime)
                            strMessage = string.Format("========= >> {0} >> " + (log.dateTime).ToString("MM-dd-yyyy hh:mm:ss tt") + " >> END >> =========|", log.logMessage);
                        else
                            strMessage = string.Format("========= >> {0} >> END >> =========|", log.logMessage);

                        sbFormattedLog.Append(Repeat("*", strMessage.Length));
                        sbFormattedLog.Append(Environment.NewLine + strMessage);
                        sbFormattedLog.Append(Environment.NewLine + Repeat("*", strMessage.Length));
                        sbFormattedLog.Append(Environment.NewLine);
                        break;
                    case "DebugStart":
                        if (log.wantToWriteDateTime)
                            strMessage = string.Format("========= >> {0} >> " + (log.dateTime).ToString("MM-dd-yyyy hh:mm:ss tt") + " >> START", log.logMessage);
                        else
                            strMessage = string.Format("========= >> {0} >> START", log.logMessage);

                        sbFormattedLog.Append(strMessage);
                        break;
                    case "Debug":
                        if (log.wantToWriteDateTime)
                            strMessage = (log.dateTime).ToString("MM-dd-yyyy hh:mm:ss tt") + string.Format(" | {0}", log.logMessage);
                        else
                            strMessage = string.Format("{0}", log.logMessage);

                        sbFormattedLog.Append(strMessage);
                        break;
                    case "DebugEnd":
                        if (log.wantToWriteDateTime)
                            strMessage = string.Format("========= >> {0} >> " + (log.dateTime).ToString("MM-dd-yyyy hh:mm:ss tt") + " >> END", log.logMessage);
                        else
                            strMessage = string.Format("========= >> {0} >> END", log.logMessage);

                        sbFormattedLog.Append(strMessage);
                        break;
                    case "Warn":
                        if (log.wantToWriteDateTime)
                            strMessage = string.Format("========= >> {0} >> " + (log.dateTime).ToString("MM-dd-yyyy hh:mm:ss tt") + " >> WARNING >> =========|", log.logMessage);
                        else
                            strMessage = string.Format("========= >> {0} >> WARNING >> =========|", log.logMessage);

                        sbFormattedLog.Append(strMessage);
                        sbFormattedLog.Append(Environment.NewLine);
                        _listOfWarnLogs.Add(log);
                        break;
                    case "Error":
                        Exception ex = log.logException;
                        sbFormattedLog.Append(string.Format("========= >> {0} : Exception/Error >> START >> =======|", ((System.Reflection.MemberInfo)(ex.TargetSite)).Name));
                        sbFormattedLog.Append(Environment.NewLine + "DateTime : " + (log.dateTime).ToString("MM-dd-yyyy hh:mm:ss tt"));
                        sbFormattedLog.Append(Environment.NewLine + "LogMessage : " + log.logMessage);
                        sbFormattedLog.Append(Environment.NewLine + "ExceptionMessage : " + Convert.ToString(ex.Message).Trim());
                        sbFormattedLog.Append(Environment.NewLine + "StackTrace : " + Convert.ToString(ex.StackTrace).Trim());
                        sbFormattedLog.Append(Environment.NewLine + string.Format("========= >> {0} : Exception/Error >> END >> =========|", ((System.Reflection.MemberInfo)(ex.TargetSite)).Name));
                        sbFormattedLog.Append(Environment.NewLine);
                        _listOfErrorLogs.Add(log);
                        break;
                    default:
                        strMessage = (log.dateTime).ToString("MM-dd-yyyy hh:mm:ss tt") + string.Format(" | {0}", log.logMessage);
                        sbFormattedLog.Append(Environment.NewLine + strMessage);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sbFormattedLog;
        }

        /// <summary>
        /// Uses to make string.
        /// </summary>
        private static string Repeat(string value, int count)
        {
            return new StringBuilder(value.Length * count).Insert(0, value, count).ToString();
        }
        #endregion

        #region Destructor
        /// <summary>
        /// The Garbage Collection mechanism in .NET will call this method prior to the garbage collection of the objects LogWriter class.
        /// </summary>
        ~LogWriter() { }
        #endregion
    }
}
